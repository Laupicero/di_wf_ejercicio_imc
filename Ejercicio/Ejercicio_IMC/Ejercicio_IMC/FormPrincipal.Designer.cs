﻿
namespace Ejercicio_IMC
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTitulo = new System.Windows.Forms.Label();
            this.labelAltura = new System.Windows.Forms.Label();
            this.labelPeso = new System.Windows.Forms.Label();
            this.pictureBoxIMGPeso = new System.Windows.Forms.PictureBox();
            this.trackBarAltura = new System.Windows.Forms.TrackBar();
            this.vScrollBarPeso = new System.Windows.Forms.VScrollBar();
            this.tbAltura = new System.Windows.Forms.TextBox();
            this.tbPeso = new System.Windows.Forms.TextBox();
            this.textBoxResultado = new System.Windows.Forms.TextBox();
            this.label4Resultado = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIMGPeso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarAltura)).BeginInit();
            this.SuspendLayout();
            // 
            // labelTitulo
            // 
            this.labelTitulo.AutoSize = true;
            this.labelTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitulo.Location = new System.Drawing.Point(25, 19);
            this.labelTitulo.Name = "labelTitulo";
            this.labelTitulo.Size = new System.Drawing.Size(529, 33);
            this.labelTitulo.TabIndex = 0;
            this.labelTitulo.Text = "ÍNDICE DE MASA CORPORAL - IMC";
            // 
            // labelAltura
            // 
            this.labelAltura.AutoSize = true;
            this.labelAltura.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAltura.Location = new System.Drawing.Point(58, 95);
            this.labelAltura.Name = "labelAltura";
            this.labelAltura.Size = new System.Drawing.Size(85, 13);
            this.labelAltura.TabIndex = 1;
            this.labelAltura.Text = "ALTURA (Cm)";
            // 
            // labelPeso
            // 
            this.labelPeso.AutoSize = true;
            this.labelPeso.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPeso.Location = new System.Drawing.Point(252, 95);
            this.labelPeso.Name = "labelPeso";
            this.labelPeso.Size = new System.Drawing.Size(67, 13);
            this.labelPeso.TabIndex = 2;
            this.labelPeso.Text = "PESO (Kg)";
            // 
            // pictureBoxIMGPeso
            // 
            this.pictureBoxIMGPeso.Image = global::Ejercicio_IMC.Properties.Resources.Sobrepeso;
            this.pictureBoxIMGPeso.Location = new System.Drawing.Point(458, 95);
            this.pictureBoxIMGPeso.Name = "pictureBoxIMGPeso";
            this.pictureBoxIMGPeso.Size = new System.Drawing.Size(106, 286);
            this.pictureBoxIMGPeso.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxIMGPeso.TabIndex = 3;
            this.pictureBoxIMGPeso.TabStop = false;
            // 
            // trackBarAltura
            // 
            this.trackBarAltura.Location = new System.Drawing.Point(78, 118);
            this.trackBarAltura.Maximum = 210;
            this.trackBarAltura.Minimum = 80;
            this.trackBarAltura.Name = "trackBarAltura";
            this.trackBarAltura.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBarAltura.Size = new System.Drawing.Size(45, 246);
            this.trackBarAltura.TabIndex = 4;
            this.trackBarAltura.Value = 160;
            this.trackBarAltura.Scroll += new System.EventHandler(this.trackBarAltura_Scroll);
            // 
            // vScrollBarPeso
            // 
            this.vScrollBarPeso.Location = new System.Drawing.Point(275, 118);
            this.vScrollBarPeso.Maximum = 220;
            this.vScrollBarPeso.Minimum = 20;
            this.vScrollBarPeso.Name = "vScrollBarPeso";
            this.vScrollBarPeso.Size = new System.Drawing.Size(29, 246);
            this.vScrollBarPeso.TabIndex = 5;
            this.vScrollBarPeso.Value = 60;
            this.vScrollBarPeso.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vScrollBarPeso_Scroll);
            // 
            // tbAltura
            // 
            this.tbAltura.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAltura.Location = new System.Drawing.Point(43, 386);
            this.tbAltura.Name = "tbAltura";
            this.tbAltura.Size = new System.Drawing.Size(100, 24);
            this.tbAltura.TabIndex = 6;
            this.tbAltura.TextChanged += new System.EventHandler(this.tbAltura_TextChanged);
            this.tbAltura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAltura_KeyPress);
            // 
            // tbPeso
            // 
            this.tbPeso.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPeso.Location = new System.Drawing.Point(241, 386);
            this.tbPeso.Name = "tbPeso";
            this.tbPeso.Size = new System.Drawing.Size(100, 24);
            this.tbPeso.TabIndex = 7;
            this.tbPeso.TextChanged += new System.EventHandler(this.tbPeso_TextChanged);
            this.tbPeso.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPeso_KeyPress);
            // 
            // textBoxResultado
            // 
            this.textBoxResultado.Enabled = false;
            this.textBoxResultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxResultado.Location = new System.Drawing.Point(458, 387);
            this.textBoxResultado.Name = "textBoxResultado";
            this.textBoxResultado.Size = new System.Drawing.Size(119, 24);
            this.textBoxResultado.TabIndex = 8;
            // 
            // label4Resultado
            // 
            this.label4Resultado.AutoSize = true;
            this.label4Resultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4Resultado.Location = new System.Drawing.Point(472, 79);
            this.label4Resultado.Name = "label4Resultado";
            this.label4Resultado.Size = new System.Drawing.Size(82, 13);
            this.label4Resultado.TabIndex = 9;
            this.label4Resultado.Text = "RESULTADO";
            // 
            // FormPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(222)))), ((int)(((byte)(206)))));
            this.ClientSize = new System.Drawing.Size(653, 450);
            this.Controls.Add(this.label4Resultado);
            this.Controls.Add(this.textBoxResultado);
            this.Controls.Add(this.tbPeso);
            this.Controls.Add(this.tbAltura);
            this.Controls.Add(this.vScrollBarPeso);
            this.Controls.Add(this.trackBarAltura);
            this.Controls.Add(this.pictureBoxIMGPeso);
            this.Controls.Add(this.labelPeso);
            this.Controls.Add(this.labelAltura);
            this.Controls.Add(this.labelTitulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FormPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Índice de masa corporal - IMC";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIMGPeso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarAltura)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitulo;
        private System.Windows.Forms.Label labelAltura;
        private System.Windows.Forms.Label labelPeso;
        private System.Windows.Forms.PictureBox pictureBoxIMGPeso;
        private System.Windows.Forms.TrackBar trackBarAltura;
        private System.Windows.Forms.VScrollBar vScrollBarPeso;
        private System.Windows.Forms.TextBox tbAltura;
        private System.Windows.Forms.TextBox tbPeso;
        private System.Windows.Forms.TextBox textBoxResultado;
        private System.Windows.Forms.Label label4Resultado;
    }
}

