﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio_IMC
{
    public partial class FormPrincipal : Form
    {

        public FormPrincipal()
        {
            InitializeComponent();
            tbAltura.Text = trackBarAltura.Value.ToString();
            tbPeso.Text = vScrollBarPeso.Value.ToString();
            calcularIMC();
        }

        //------------------------------------------
        //Cambio de valores con track/Scroll-Bar
        //------------------------------------------
        //Al cambiar el la altura con el 'trackbar'
        private void trackBarAltura_Scroll(object sender, EventArgs e)
        {
            tbAltura.Text = trackBarAltura.Value.ToString();
            calcularIMC();
        }

        //Al cambiar el peso con el 'vScrollBar'
        private void vScrollBarPeso_Scroll(object sender, ScrollEventArgs e)
        {
            tbPeso.Text = vScrollBarPeso.Value.ToString();
            calcularIMC();
        }


        //---------------------------------------------------------
        // Cambio de valores con los textBox correspondientes
        //---------------------------------------------------------

        //--------------
        // ALTURA
        //--------------
        //Evento keyPress
        private void tbAltura_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (tbAltura.TextLength > 0)
            {
                if ((e.KeyChar >= '0' && e.KeyChar <= '9') || Char.IsControl(e.KeyChar))
                {
                    e.Handled = false;

                }
                else
                {
                    e.Handled = true;
                }
            }
        }

        //Evento textChanged
        private void tbAltura_TextChanged(object sender, EventArgs e)
        {
            if (tbPeso.TextLength > 0 && tbAltura.TextLength > 0)
            {
                int alturaCM = Convert.ToInt32(tbAltura.Text);

                if (alturaCM >= 80 && alturaCM <= 210)
                {
                    trackBarAltura.Value = alturaCM;
                    calcularIMC();
                }
            }
        }

        //--------------
        // PESO
        //--------------

        //KeyPress
        private void tbPeso_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (tbPeso.TextLength > 0)
            {
                if ((e.KeyChar >= '0' && e.KeyChar <= '9') || Char.IsControl(e.KeyChar))
                {
                    e.Handled = false;

                }
                else
                {
                    e.Handled = true;
                }
            }
        }

        //TextChanged
        private void tbPeso_TextChanged(object sender, EventArgs e)
        {
            if (tbPeso.TextLength > 0 && tbAltura.TextLength > 0)
            {
                int pesoKg = Convert.ToInt32(tbPeso.Text);

                if (pesoKg >= 80 && pesoKg <= 210)
                {
                    vScrollBarPeso.Value = pesoKg;
                    calcularIMC();
                }
            }
        }


        //--------------------------------------
        // MÉTODOS PARA EL CALCULO DEL IMC
        //--------------------------------------

        //Nos calculará el índice de masa corporal
        private void calcularIMC()
        {
            Double peso = Convert.ToDouble(tbPeso.Text);
            Double alturaM = (Convert.ToDouble(tbAltura.Text)) / 100;

            Double resultado = peso / (Math.Pow(alturaM, 2));

            //Limitamos el número decimal a sólo 3 decimales
            textBoxResultado.Text = string.Format("{0:0.###}", resultado);

            cambiarImagenPicBox(resultado);
        }

        
        //Método que nos cambiará la imagen del PicxtureBox
        private void cambiarImagenPicBox(double resultado)
        {
            if(resultado < 18.5)
            {
                pictureBoxIMGPeso.Image = Properties.Resources.PesoBajo;
            
            }else if (resultado >= 18.5 && resultado <= 24.9)
            {
                pictureBoxIMGPeso.Image = Properties.Resources.PesoIdeal;
            }
            else if (resultado >= 25 && resultado <= 29.9)
            {
                pictureBoxIMGPeso.Image = Properties.Resources.Sobrepeso;
            }
            else if (resultado >= 30 && resultado <= 34.9)
            {
                pictureBoxIMGPeso.Image = Properties.Resources.Obeso;
            }
            else if (resultado >= 35 && resultado <= 39.9)
            {
                pictureBoxIMGPeso.Image = Properties.Resources.ObesidadSevera;
            }
            else if (resultado >= 40)
            {
                pictureBoxIMGPeso.Image = Properties.Resources.ObesidadMorbida;
            }
        }        
    }
}
